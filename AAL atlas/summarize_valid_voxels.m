%When we apply an atlas onto subject's space there are sometimes missing values (NA)
%This code summarizes the valid voxels after the AAL atlas has been applied 


sub_dir = '/home/.bml/Data/Bank1/Ashley/Codes/Subject_lists/';
load(strcat(sub_dir, 'allsubj.mat'));
subj = sub;  
%% pre-allocate vvmat array (valid voxel matrix)
vvmat = zeros(length(subj), 90);
%% looping each subject
for i=1:length(subj)
    % identify potential file name
    ROI_masks=dir(sprintf('/home/.bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/ROIs_SubjSpace/*.nii',subj(i)));         

    % get roi mask path
    fullpaths=cell(90,1); %this makes a cell with the 90 masks for each subj
    ROI_names=cell(90,1);
    roi_c = 0;
    for u=1:length(ROI_masks)
        % make sure the included mask names start with w and then 
        % capitalized letter.
        % "Amygdala_L.nii" should be included, but "wAmygdala.nii" should
        % not be included.
        if regexp(ROI_masks(u).name, "^w[A-Z].+.nii")
            roi_c = roi_c + 1;
            fullpaths{roi_c}=strcat(ROI_masks(u).folder,'/',ROI_masks(u).name);
            matched = regexp(ROI_masks(u).name, "^w([A-Z].+).nii", "tokens");
            ROI_names{roi_c} = matched{1}{1};
        end
    end
    
    % read image and output number of voxel greater than 0.5
    V = spm_vol(fullpaths);
    for i_roi=1:length(V)
        Y = spm_read_vols(V{i_roi});
        n_validvox = length(find(Y >= 0.5));
        vvmat(i, i_roi) = n_validvox;
    end
end

%% output csv file
fid = fopen('/bml/Data/Bank1/Ashley/Codes/AAL_atlas_normalize/validvox_table.csv', 'w+');
% write header
fprintf(fid, "Subject");
for i_roi=1:length(ROI_names)
    fprintf(fid, ",%s", ROI_names{i_roi});
end
fprintf(fid, "\n");
% write each row
for i_subj=1:length(subj)
    fprintf(fid, "%d", subj(i_subj));
    for i_roi=1:length(ROI_names)
        fprintf(fid, ",%d", vvmat(i_subj, i_roi));
    end
    fprintf(fid, "\n");
end
% close the file
fclose(fid);
