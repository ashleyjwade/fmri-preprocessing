%Code made to make the 90 ROI masks by name and number that is assigned
%from the AAL atlas (ex: 5402, 'Fusiform_R')
%when checking that these masks are accurate, use
%'/usr/local/spm12_v7771/canonical/single_subj_T1.nii' as the first image
%and then check each mask 
fid = fopen('/home/.bml/Data/Bank1/Ashley/atlas/roi_list.csv'); %to load the list that was made for each value number assigned to the ROIs
M = textscan(fid, '%d%s', 'Delimiter', ',');
fclose(fid)
%%
fname = '/home/.bml/Data/Bank1/Ashley/atlas/AAL.nii'; %filename directory of the AAL atlas 
V = spm_vol(fname); %this file is now a structure array containing the image information so that SPM can read it 
Y = spm_read_vols(V); %this is to read the entire image volumes, read the 'V' that was created to create the masks 
%%
for i = 1:length(M{1}) %this is the file of each number (M1) and each corresponding ROI (M2) 
    roi_id = M{1}(i);
    roi_name = M{2}{i};

    new_Y = double(Y == roi_id); %this is used so that the V&Y don't get overwritten 
    new_V = V;
    
    new_V.fname = ['/home/.bml/Data/Bank1/Ashley/atlas/', roi_name '.nii'];

    spm_write_vol(new_V, double(new_Y))
end
%%

