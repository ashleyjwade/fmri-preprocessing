%% SPM_T1_check_reg.m %%
%
% Usage:
% - For those new to MATLAB, the '%' sign is how we comment things here. If
%   two '%' signs are put together--'%%' we can create a 'section' of code.
%   This script is written to be run section-by-section. You can press 
%   "control+enter" to evaluate/execute the section your cursor is at.
%
% - This script is written as a more convenient way to view the T1 files
%   with the SPM check registration function. With several keyboard
%   presses, you can review the T1 files batch by batch easily without
%   using your mouse.
%
% - Ideally, you will only have to change the variables in step 1. Step 0 is
%   an optional step that exhibits the heart of the code. Check the command
%   window after running step 2 to see if your variables are set as you
%   wanted. Run step 4 repeatedly to check through you T1 files.
% 
% - This script is written under that assumption that you are naming the
%   files for each subjet with the format 'abbreviation+id' (e.g. ADM001,MIFC0053 etc). 
%   Also, I assumed that the T1s are stored directly the subject directory.
%   (i.e. /bml/Data/Bank1/MYPROJECT/derivatives/MYPROJECT0001/T1/MYPROJECT0001_MPRAGE_.nii)
%   You might have to change the codes a little bit more if your files are
%   not stored under that assumption.
%
% Version History:
% - 2021/09/09: Created by Jess CC Hsing with inspiration from UL Hsieh
% - 2021/09/13: Modified by Jess CC Hsing to fetch for dynamic T1 names
%%--------------------%%
%% Step 0: The logic for the script

%The spm_select prompt that allows you to click the path to files and save the path as string in variable 'P' 
P = spm_select(Inf,'image','Select images',[],pwd); 
%The SPM command that calls spm to perform check registration
spm_check_registration(deblank(P)); 

%% Step 1: Set up these variables for your project
load('/bml/Data/Bank1/Ashley/Codes/subjectlist.mat')
% Enter the directory where you sotre all your subject's files
filepath = '/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii'; 
% Enter the abbreviation for your project
projectname = '';                                
% Enter the format of you T1 file names, only type in the part that are EXACTLY the same across subject
%  wherever there is a part that varies across subject, replace the different parts with a * (wildcard sign)
T1fileformat = '_MPRAGE_256_*.nii';                
% Specify the size of the batch for each check registration session, the maximum is 24
batch = 6;    

subid=sub';

%%
T1file = cell(1,length(subid));
for k = 1:length(subid)
T1fileres = dir(sprintf('%s/%d/%d%s',filepath,subid(k),subid(k),T1fileformat));
T1file(k) = {T1fileres.name};

end
    
%% Step 2: Run This section to check if you did step 1 right
i = 1;
fprintf('The T1 file for %s is at:\n %s%s/T1/%s \n',subid{i},filepath,subid{i},T1file{i})

%% Step 3: Execute this section to create char string K forthe next step
nsub = length(subid); 
T1files = cell(nsub,1); % Preallocation

% Create cell array that saves T1 path for each subject
for i = 1:nsub
T1files{i} = sprintf('%s/%d/%s',filepath,subid(i),T1file{i});
end

K = char(deblank(T1files)); % Convert string cell to char so that SPM can read
k = 0; % Set counter k for step 4

% This part divides the number of subjects to the size of each batch to
% ensure smooth execution
R = rem(nsub,batch);
Q = (nsub-R)/batch;
n = mat2cell(1:nsub,1,[repmat(batch,1,Q), R]);
%% Step 4: Run this section repeatedly to view a new batch
k = k+1;
spm_check_registration(K(n{k},:));
