%Check subject's rp.txt file for motion correction 

%load subject list
sub_dir = '/home/.bml/Data/Bank1/Ashley/Codes/Subject_lists/';
load(strcat(sub_dir, 'allsubj.mat'));
subj = sub;

%main directory
maindir = '/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii';
for i = 1:length(subj)
    rp_paras = {};
    rp_filename = dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/rp_%d_ep2d_moco_3mm_180_*.txt',subj(i),subj(i)));
    str = string(rp_filename.folder);
    ing = string(rp_filename.name);
    both = strcat(str,'/',ing);
    rp_paras =[rp_paras, load(both)];
    %rp_filename = [maindir '/' num2str(subj) '/' 'rp_' num2str(subj) '_ep2d_moco_3mm_180_20110409105955_5.txt'];
    %rp_paras = {sprintf('%s/%s',rp_filename(i).folder, rp_filename(i).name)};
    %rp_paras = [rp_paras, load(rp_filename)];
    
A = vertcat(rp_paras{:});
vol_num = size(A);

T(i) = figure(i);
plot(1:vol_num(1), A(1:vol_num(1),1), 1:vol_num(1), A(1:vol_num(1),2), 1:vol_num(1), A(1:vol_num(1),3))
xline(vol_num(1)/4)
xline(vol_num(1)/4*2)
xline(vol_num(1)/4*3)
xline(vol_num(1))
figurename = {};
figurename = [figurename, ['REST_translation' num2str(i)]];
saveas(T(i), char(figurename), 'png')

R(i) = figure(i);
plot(1:vol_num(1), A(1:vol_num(1),4), 1:vol_num(1), A(1:vol_num(1),5), 1:vol_num(1), A(1:vol_num(1),6))
xline(vol_num(1)/4)
xline(vol_num(1)/4*2)
xline(vol_num(1)/4*3)
xline(vol_num(1))
figurename = {};
figurename = [figurename, ['REST_rotation' num2str(i)]];
saveas(R(i), char(figurename), 'png')

end 
