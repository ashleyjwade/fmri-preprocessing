%REALIGN: Estimate and Reslice
%FOR LOOP 2, you wrote loop 2 because you ran coreg and realign together in
%a batch
epi_files=cell(180,1); %this is how you preallocate, you are opening a cell in the workspace that can easily use the images you are wanting in the code, because these files are large, it is better to keep them 'handy' in the cell so it doesn't take too long or take up too much memory/space
for img=1:180 %you wrote 'img' because you need a new name since you have already used 'i'
    epi_files{img}=sprintf('%s/%s,%d',EPIfilenames(i).folder,EPIfilenames(i).name,img); %at the end of this script you wrote ',img' because there are 180 of these (which you specified in the beginning of the for loop 
end %{} function creates a cell 
matlabbatch{2}.spm.spatial.realign.estwrite.data = {epi_files}; %you use the {} because it is in the workspace cell you created 
%%
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.quality = 0.9;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.sep = 4;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.fwhm = 5;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.interp = 2;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.wrap = [0 0 0];
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.weight = '';
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.which = [2 1];
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.interp = 4;
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.wrap = [0 0 0];
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.mask = 1;
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
save(batchname,'matlabbatch') %save the matlabbatch because you need to open the files created in the SPM batch editor, check them, then run it

end
