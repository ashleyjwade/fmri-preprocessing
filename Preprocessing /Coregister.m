%COREGISTER 
%FOR LOOP 1
%the for loop for creating a cell list for all of the T1 images, by
%replacing the subject number based on the subjlist that you created before
%you do this so that you can create a 'cell' in the workspace that will
%update the new subject numbers that you wrote %d for, this is how you get
%the for loop to organize the files that you want it to process
%(participant number change in script) 

%the for loop for creating a cell list for all of the EPI images, by
%replacing all of the things that differ between subject file names
sub_dir = '/home/.bml/Data/Bank1/Ashley/Codes/Subject_lists/';
load(strcat(sub_dir, 'allsubj.mat'));
subj = sub;
subj=sub(1:end); %this is because we create a list of the subjects that have T1's and EPI's, if they don't then we coded them to be left out i=1:length(subj)
    
for i=1:length(subj)
    batchname=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_preprocess1/coreg_realign_preprocess_%d',subj(i));
    %create the batchname folder and write the path where you want this
    %folder to be located 
    EPIfilenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/*_ep2d_moco_3mm_180_*_*.nii',subj(i)));
    %this creates the script for the EPI's cell that you created in the
    %workspace
    T1filenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/*_MPRAGE_256_*_*.nii',subj(i)));
    %this creates the script for the T1's cell that you created in the
    %workspace 
matlabbatch{1}.spm.spatial.coreg.estimate.ref = {sprintf('%s/%s,1',EPIfilenames(i).folder,EPIfilenames(i).name)}; %this is one way to name the script that will be updated in the for loop, you need to add a ',1' because each EPI will have 1:180 image files within each aubject's EPI but for this step you only need 'EPI,1'
matlabbatch{1}.spm.spatial.coreg.estimate.source = {fullfile(T1filenames(i).folder,T1filenames(i).name)}; %this is another way to name the script that will be updated in the for loop 
matlabbatch{1}.spm.spatial.coreg.estimate.other = {''};
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.cost_fun = 'nmi';
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.sep = [4 2];
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.fwhm = [7 7];
%%
save(batchname,'matlabbatch') 
end 
