%SLICETIME 
%preallocate before the for loop
sub_dir = '/home/.bml/Data/Bank1/Ashley/Codes/';
load(strcat(sub_dir, 'subjectlist.mat'));
subj = sub;

for i=1:length(subj) %you wrote it like this because if you want to update the 'subj' list then all you have to do is run the code again 
    batchname=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_preprocess2/slicetime_segment_preprocess_%d',subj(i)); %this is a new folder for the new batch
    realignedEPIfilenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/r*_ep2d_moco_3mm_180_*_*.nii',subj(i)));
    rEPI_files=cell(180,1);
    for u=1:180
        rEPI_files{u}=sprintf('%s/%s,%d',realignedEPIfilenames(i).folder,realignedEPIfilenames(i).name,u); %this is a new file with the prefix 'r' because they have been realigned already 
    end 
    
matlabbatch{1}.spm.temporal.st.scans = {rEPI_files};
%%
matlabbatch{1}.spm.temporal.st.nslices = 34;
matlabbatch{1}.spm.temporal.st.tr = 2;
matlabbatch{1}.spm.temporal.st.ta = 1.94117647058824;
matlabbatch{1}.spm.temporal.st.so = [2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 1 3 5 7 9 11 13 15 17 19 21 23 25 27 29 31 33];
matlabbatch{1}.spm.temporal.st.refslice = 2;
matlabbatch{1}.spm.temporal.st.prefix = 'a';
%
save(batchname,'matlabbatch') 
end 
