%%The original data has been already downloaded from LibreOffice "rsfMRIfiletypesSheet1"
%%To make a subject lists from csv file 
 csvfile=table2array(rsfMRIfiletypesSheet1); %since the file type is a table you need to make it into an array, double click and open the rs-fmrifiletypesheet and then "import" 
 %we want to filter out the subjects that don't have T1 files or EPI files 
 allsubs=csvfile(:,1); %this writes the file (allsubs) with the subject numbers 
 t1status=csvfile(:,4); %this writes the T1 value (position row 4) as 0 or 1 
 sub=allsubs(logical(t1status)); %this adds the logical T1s to the file (allsubs) 
 episubs=csvfile(:,6); %this writes the EPI value (position row 6) as 0 or 1 
 sub=allsubs(logical(episubs)); %this adds the logical EPIs to the file (allsubs)
 filter=and(t1status,episubs); %this filters the 0 values of T1 and EPI and 'adds' them together using 'and'
 sub=allsubs(filter); %this adds the filter you just used and adds it to the new file 'sub' 
 
%%Separate ASD and TD lists and organize by age 
csvfile=table2array(rsfMRIfiletypesSheet1);
allsubs=csvfile(:,1);
ASDsubs=sub(1:90);
TDsubs=sub(91:195);
agestatus=csvfile(:,3);%separate age of each subj then add to ASDsubs & TDsubs
ASDagestatus=agestatus(1:90);
TDageastatus=agestatus(91:195);
ASD=[ASDsubs ASDagestatus];  %combines ASD subject numbers and their age 
TD=[TDsubs TDageastatus];    %combines TD subject numbers and their age 

csvwrite('ASD.csv',ASD)
csvwrite('TD.csv',TD)
