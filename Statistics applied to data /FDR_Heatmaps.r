#GRAPHS
```{r}
#GRAPHS
#Prepare data for heatmap
Matrix_roi <- matrix(unlist(Data.statistics.ROI.order.p), ncol = 90, byrow = FALSE) #unlist because the function 'heatmap' does not allow a list, you need a matrix 
```

#Mulitple comparisons test
##FDR
```{r}
#FDR correction shows which pvalues are "real"
apfdr<-p.adjust(Matrix_roi, method="fdr", n=length(Matrix_roi))
apfdr<-matrix(apfdr,nrow=90,ncol=90)
#apfdr[!is.finite(apfdr)] <- 0 #this makes all NA values equal to 0
write.csv(apfdr,'fdr.csv') #save csv file with the fdr corrected pvalues 

#FDR values to accept-->less than 0.05 score 
loc_list<-which((apfdr<0.05)&(apfdr!=0), arr.ind=TRUE) #to print the values <0.05 and not 0
Data.statistics.ROI.order.t[loc_list]

#make an empty 90x90 matrix 
Res <- matrix(0, nrow=nrow(Data.statistics.ROI.order.t), ncol=ncol(Data.statistics.ROI.order.t))
#using the location from loc_list, then putting the same value at the location from t-value matrix, into one matrix 
Res[loc_list] <- Data.statistics.ROI.order.t[loc_list]
Res<-matrix(unlist(Res), ncol = 90, byrow = FALSE)

#row and column labels 
cortices <- read.csv("/home/ashley/Workspace/Subject_lists/cortices.csv", header=FALSE)
Cortical_orders<-read.csv("/home/ashley/Workspace/Subject_lists/ROI_groups - ALL_ROI_sort.csv", header=F)
Cortical_orders<- sub(".nii","",Cortical_orders$V1)
Cortical_orders<- sub("'","",Cortical_orders)

#add row and column names to the file 
rownames(Res) <- Cortical_orders
colnames(Res) <- Cortical_orders

```
##Heatmap after FDR of t-values 
```{r}
library(gplots)
# png("heatmap_ASD-TD(fdr).png",width=500, height=500)
# 
# heatmap.2(Res, Rowv=FALSE, Colv=FALSE, col=bluered, scale="none", tracecol="white",
#           margin=c(6,8),
#           xlab="ROI cortices", ylab= "Specific ROIs",
#           main="ASD-TD T-values (p(fdr)<0.05)", cexRow=0.5, cexCol=1,
#           srtRow=360, srtCol=45, adjCol=c(1,1), adjRow=c(0,1),
#           key.title="T-values", lhei=c(1.5,4.5),
#           lwid <- c(1.5, 4, 6))

library(pheatmap)
library(RColorBrewer)

groups<-read.csv("/home/ashley/Workspace/Subject_lists/groups.csv", header=FALSE) #names of cortical groups 
mydf<-data.frame(row.names=1:length(Cortical_orders), Category=groups)
colnames(mydf) <- c("Category")
rownames(mydf) <- Cortical_orders
newCols <- colorRampPalette(grDevices::rainbow(length(unique(mydf$Category))))
mycolors <- newCols(length(unique(mydf$Category)))
names(mycolors) <- unique(mydf$Category)
mycolors <- list(Category = mycolors)
fsize <- 7

Res[Res == 0] <- NA

pheatmap(Res, color = colorRampPalette(rev(brewer.pal(n = 6, name =
  "RdBu")))(100), kmeans_k = NA, breaks = NA, border_color = NA, width = 15, height = 18,
  cellwidth = fsize, cellheight = fsize, scale = "none", cluster_rows = FALSE,
  cluster_cols = FALSE, legend = TRUE, legend_breaks = c(-6, -3, 0, 3, 6, max(Restd)),
  legend_labels = c("-6","-3","0","3","6","T-scores\n"), annotation_row = mydf, annotation_col = 
    mydf, annotation_colors = mycolors, annotation_legend = TRUE,
  annotation_names_row = TRUE, annotation_names_col = TRUE,
  drop_levels = TRUE, show_rownames = T, show_colnames = T, main = "ASD-TD_T-scores from p(fdr)<0.05",
  angle_col = c("90"), fontsize= 17, fontsize_col = fsize, fontsize_row = fsize, cex= 1, display_numbers = F,
   gaps_row = NULL, gaps_col = NULL, labels_row = Cortical_orders,
  labels_col = Cortical_orders, na_col = "white", filename =
    "/home/ashley/Workspace/T_test/ASD-TD_T_score_p(fdr)<0.05.png")


```
