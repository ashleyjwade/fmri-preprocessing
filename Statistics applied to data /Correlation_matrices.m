%%Correlation Matrices 
%%taking the voxel_values_%d.mat file and converting it into matrix to use
%%the command 'corrcoef' to make a 90x90 matrix for each subject 

for i=1:length(subj)
    %need to take the .mat file created from CatCarryingVoxel and transfer to
    %cell then to matrix, tranpose it so that the 90 ROI's are in the columns
    %and the 180 time series Residuals are in the rows 
    voxels_mat=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/Voxel_values_%d.mat',subj(i),subj(i));
    %dir Voxel_values_*.mat
    S=load(voxels_mat,'-mat');
    C=struct2cell(S);
    D=cell2mat(C); %need to transfer from cell to matrix to use 'corrcoef'
    D_T=D'; %tranpose to put the 90ROI's at the top  
   
    batchname=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/Correlation_matrix_%d',subj(i),subj(i));
    
    % 
    % %ROI label names added to the cell array 
    % ROI_names={mask.name}'; %' is to transpose 
    % Final=[ROI_names,C];
    % Final_transpose=Final'; 

    iR=corrcoef(D_T(1:end,:),'Rows','pairwise'); 
    save(batchname,'iR')
    csvwrite(sprintf('Correlation_matrix_%d.csv',subj(i)),iR);
    clearvars -except sub_dir subj sub i
end


    batchname=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/Correlation_matrices_%d',subj(i));
    csvwrite('Correlation_matrix_%d.csv',iR);
