#ROI Cortical order-ttest 
##ASD
```{r}
csvs_u<-lapply(list.files(path="/home/ashley/Workspace/Z_scores/ASD/ROI_order/All_subj_new_ROI_order", full.names = T),read.csv)

  for (i in 1:length(csvs_u)){ #this for loop is to loop through the subjects 90x90s because it is in a list from read.csv 
    csvs_u[[i]]<-csvs_u[[i]][,2:ncol(csvs_u[[i]])] #this is to remove the first col of ROI names in the first row 
  }

csvs_u.matrix <- csvs_u %>%
  map(function(x) as.matrix(x)) #this makes the csvs_u list into a matrix 

subj <- paste0("csvs_u.matrix[[",1:length(csvs_u),"]]") #this is adding "csvs_u.matrix[[]]" to each csv file number in the subject list so that we can call for it later in the t-test function 

# Create text so that the files are in 'string' format 
text.subj <- paste0("paste0(\'c(\',", paste(subj, collapse = ",\',\',"),",\')\')")

# Evaluate the previous text to combine matrices/CSVs
ASDttest <- matrix(eval(parse(text = text.subj)),nrow = 90) %>%
        map(function(x) eval(parse(text = x))) %>%
        matrix(nrow = 90)
  
ASDttest[lower.tri(ASDttest,diag=TRUE)] <-0 #everything in upper triangle of matrix is 0, we do this so that the t-test function will run without the problem of NA values in the diaganol 
```
##TD
```{r}
# Create a pseudo data of TD participants
csvs_u_td<-lapply(list.files(path="/home/ashley/Workspace/Z_scores/TD/ROI_order/All_subj_new_ROI_order", full.names = T), read.csv) 

 for (i in 1:length(csvs_u_td)){ #this for loop is to loop through the subjects 90x90s because it is in a list from read.csv 
    csvs_u_td[[i]]<-csvs_u_td[[i]][,2:ncol(csvs_u_td[[i]])] #this is to remove the first col of ROI names 
 }

csvs_u_td.matrix <- csvs_u_td %>%
  map(function(x) as.matrix(x))  #to make a matrix 

subj_td <- paste0("csvs_u_td.matrix[[",1:length(csvs_u_td),"]]") #to add names to the csvs 
text.subj_td <- paste0("paste0(\'c(\',", paste(subj_td, collapse = ",\',\',"),",\')\')") #to make into string
TDttest <- matrix(eval(parse(text = text.subj_td)),nrow = 90) %>%
        map(function(x) eval(parse(text = x))) %>%
        matrix(nrow = 90)
TDttest[lower.tri(TDttest,diag=TRUE)] <-0 #this makes everything (including diag) above triangle to be 0
```
##Combine ASD-TD
```{r}
##Combine the ASD and TD into strings of a list of two vectors by element 
Data_ROI_order <-
        matrix(paste0('list(',ASDttest,',',TDttest,')'),nrow = 90) %>% 
        # Make the strings into real lists
        map(function(x) eval(parse(text = x))) %>% 
        # Reshape the matrix
        matrix(nrow = 90)
#Now there is a 90x90 matrix, in each element (ex:1,1) All of the zscore values from the ASD subjects at (1,1) are in the group 'ASDttest" and all of the zscore values from the TD subjects at (1,1) are in the group 'TDttest' and so on for the entire 90x90 matrix. We do this so that the t-test will be performed for each element. We do not take the average because then the t-test will not show the real differences between the two groups. 
```
#Function for Inf and return 
##T-test statistic (t-value) new ROI order 
###this function was created so that the t.test function will still perform even when all of the values (diaganol values) will be 0 or NA 
```{r}
mytest <- function(x,y) {
  if (all(x == 0) || all(y == 0)) {
    return(0)
    }
    return(t.test(x,y, paired=FALSE, na.rm=TRUE)$statistic[[1]][[1]])
} 

Data.statistics.ROI.order.t <- 
        # Open Data
        Data_ROI_order %>% 
        # Run t.tests by element (now a list of two vectors each element)
        #map(function(x) t.test(Data[[1]],Data[[2]],na.rm=TRUE)) %>%
        map(function(x) mytest(x[[1]],x[[2]])) %>%
        # Reshape the matrix
        matrix(nrow = 90)

write.csv(Data.statistics.ROI.order.t, file="ASD_TDtvalue.csv")
#group x[[1]] is ASD and group x[[2]] is TD 
```
##P-value new ROI order 
```{r}
mytest <- function(x,y) {
  if (all(x == 0) || all(y == 0)) {
    return(0)
    }
    return(t.test(x,y, paired=FALSE, na.rm=TRUE)$p.value)
} 

Data.statistics.ROI.order.p <- 
        # Open Data
        Data_ROI_order %>% 
        # Run t.tests by element (now a list of two vectors each element)
        #map(function(x) t.test(Data[[1]],Data[[2]],na.rm=TRUE)) %>%
        map(function(x) mytest(x[[1]],x[[2]])) %>%
        # Reshape the matrix
        matrix(nrow = 90)
write.csv(Data.statistics.ROI.order.p,'pvalue.csv') #this csv has the first col with 0 because it was the ROI name labels 
