#Add ROI labels
```{r}
#Run ROI_names
ROI_names_row<-t(ROI_names)
t<-t(ROI_names_row) #transpose 
```
##making cortical order list and reordering zscores 
##TD
```{r}
#TD
csvs_u_td<-list.files(path="/home/ashley/Workspace/Z_scores/TD/CSV_zscores", full.names = T)

Cortical_orders<- read.csv("/home/ashley/Workspace/Subject_lists/ROI_groups - ALL_ROI_sort.csv", header=F)
  #read.csv("/home/ashley/Workspace/Subject_lists/ROI_nameslist _ALL_ROI_sort.csv", header=FALSE)
Cortical_orders<- sub(".nii","",Cortical_orders$V1)
Cortical_orders<- sub("'","",Cortical_orders)

for (u in csvs_u_td){
  CSV_ROI_labels<-read.csv(u,header=TRUE, col.names=ROI_names, row.names=t) #this adds the ROI names in alphabetical order 
  
  data_with_labels<-CSV_ROI_labels[Cortical_orders,Cortical_orders] #this reorders the [col,row], ROI names from cortical order list 
  
  num <- strsplit(u,"_")[[1]][[5]] #splits the string in order to get subj number in filename
  age<- strsplit(u,"_")[[1]][[7]] #splits the string in order to get age of subjects 
 
  filename<-paste0("/home/ashley/Workspace/Z_scores/TD/ROI_order/All_subj_new_ROI_order/",num,"_",age,"_TD_ROI_labels.csv")
 
  write.csv(data_with_labels, filename, row.names=T)
}
```

```{r}
#ASD
csvs_u<-list.files(path="/home/ashley/Workspace/Z_scores/ASD/CSV_zscores", full.names = T)

Cortical_orders<-read.csv("/home/ashley/Workspace/Subject_lists/ROI_groups - ALL_ROI_sort.csv", header=F)
Cortical_orders<- sub(".nii","",Cortical_orders$V1)
Cortical_orders<- sub("'","",Cortical_orders)


for (u in csvs_u) {
  CSV_ROI_labels<-read.csv(u,header=TRUE, col.names=ROI_names, row.names=t) #this adds the ROI names in alphabetical order
  
  data_with_labels<-CSV_ROI_labels[Cortical_orders,Cortical_orders] #this reorders the [col,row], ROI names from cortical order list 
  
   num <- strsplit(u,"_")[[1]][[5]] #splits the string in order to get subj number in filename
  age<- strsplit(u,"_")[[1]][[7]] #splits the string in order to get age of subjects 
 
  filename<-paste0("/home/ashley/Workspace/Z_scores/ASD/ROI_order/All_subj_new_ROI_order/",num,"_",age,"_ASD_ROI_labels.csv")
  write.csv(data_with_labels, filename, row.names=T)
}
```
##apply to each subj by age 
#New ROI order graphs by age 
##TD
```{r}
#TD average of z-scores 
for (u in 9:19) {
  csvs_u_td<-list.files(path="/home/ashley/Workspace/Z_scores/TD/ROI_order/All_subj_new_ROI_order/",pattern=paste0("*_",u),full.names=TRUE)
  csvs_u_td_age <-lapply(csvs_u_td,read.csv) 
  for (i in 1:length(csvs_u_td_age)){ #this for loop is to loop through the subjects 90x90s because it is in a list from read.csv 
    csvs_u_td_age[[i]]<-csvs_u_td_age[[i]][,2:ncol(csvs_u_td_age[[i]])] #this is to remove the first col of ROI names 
  }
  Avg_TD_u<-Reduce("+", csvs_u_td_age) / length(csvs_u_td_age) #to find the average of csv files by age
  filenames<-paste0("./Workspace/Z_scores/TD/ROI_order/Avg_by_AGE_new_ROI_order/","TD_Avg_Cortical_orders",u,".csv")
  write.csv(Avg_TD_u, filenames)
  Avg_u<-print(ggcorrplot(Avg_TD_u)+ 
    labs(title = paste("AVG_TD_zscores_Age_",u,sep=""))+
    theme(axis.text.y = element_text(size=4))+
    scale_x_continuous(breaks=c(6,27,55,76,85), labels=c("Parietal","Frontal","Temporal","Occipital","Sub-Cortical"),
                       expand= c(0,0)))
  
  ggsave(Avg_u,filename=paste0("./Workspace/Z_scores/TD/ROI_order/Avg_by_AGE_new_ROI_order/","TD_Avg_zscore_",u,".png"))
}
```
##ASD
```{r}
#ASD average of z-scores 
for (u in 9:19) {
  csvs_u<-list.files(path="/home/ashley/Workspace/Z_scores/ASD/ROI_order/All_subj_new_ROI_order",pattern=paste0("*_",u),full.names=TRUE)
  csvs_u_asd_age <-lapply(csvs_u,read.csv) 
  for (i in 1:length(csvs_u_asd_age)){
    csvs_u_asd_age[[i]]<-csvs_u_asd_age[[i]][,2:ncol(csvs_u_asd_age[[i]])] #this is to remove the first col of ROI names 
  }
  Avg_ASD_u<-Reduce("+", csvs_u_asd_age) / length(csvs_u_asd_age) #to find the average of csv files by age 
  filenames<-paste("./Workspace/Z_scores/ASD/ROI_order/Avg_by_AGE_new_ROI_order/","ASD_Avg_Cortical_orders",u,".csv")
  write.csv(Avg_ASD_u, filenames)
  Avg_u<-print(ggcorrplot(Avg_ASD_u)+ 
    labs(title = paste("AVG_ASD_zscores_Age_",u,sep=""))+
    theme(axis.text.y = element_text(size=4))+
    scale_x_continuous(breaks=c(6,27,55,76,85), labels=c("Parietal","Frontal","Temporal","Occipital","Sub-Cortical"),
                       expand= c(0,0)))
  
  ggsave(Avg_u,filename=paste0("./Workspace/Z_scores/ASD/ROI_order/Avg_by_AGE_new_ROI_order/","ASD_Avg_zscore_",u,".png"))
}
```
