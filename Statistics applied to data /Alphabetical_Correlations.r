---
title: Correlation Matrices 
output: html_output
---
#Correlation Matrices

```{r}
library(ggplot2)
library(ggcorrplot)
library(DescTools)
library(readr)
library(dplyr)
 ROI_names<-c("Amygdala_L","Amygdala_R","Angular_L","Angular_R","Calcarine_L","Calcarine_R","Caudate_L","Caudate_R","Cingulum_Ant_L","Cingulum_Ant_R","Cingulum_Mid_L","Cingulum_Mid_R","Cingulum_Post_L","Cingulum_Post_R","Cuneus_L","Cuneus_R","Frontal_Inf_Oper_L","Frontal_Inf_Oper_R","Frontal_Inf_Orb_L","Frontal_Inf_Orb_R","Frontal_Inf_Tri_L","Frontal_Inf_Tri_R","Frontal_Med_Orb_L","Frontal_Med_Orb_R","Frontal_Mid_L","Frontal_Mid_Orb_L","Frontal_Mid_Orb_R","Frontal_Mid_R","Frontal_Sup_L","Frontal_Sup_Medial_L","Frontal_Sup_Medial_R","Frontal_Sup_Orb_L","Frontal_Sup_Orb_R","Frontal_Sup_R","Fusiform_L","Fusiform_R","Heschl_L","Heschl_R","Hippocampus_L","Hippocampus_R","Insula_L","Insula_R","Lingual_L","Lingual_R","Occipital_Inf_L","Occipital_Inf_R","Occipital_Mid_L","Occipital_Mid_R","Occipital_Sup_L","Occipital_Sup_R","Olfactory_L","Olfactory_R","Pallidum_L","Pallidum_R","ParaHippocampal_L","ParaHippocampal_R","Paracentral_Lobule_L","Paracentral_Lobule_R","Parietal_Inf_L","Parietal_Inf_R","Parietal_Sup_L","Parietal_Sup_R","Postcentral_L","Postcentral_R","Precentral_L","Precentral_R","Precuneus_L","Precuneus_R","Putamen_L","Putamen_R","Rectus_L","Rectus_R","Rolandic_Oper_L","Rolandic_Oper_R","Supp_Motor_Area_L","Supp_Motor_Area_R","SupraMarginal_L","SupraMarginal_R","Temporal_Inf_L","Temporal_Inf_R","Temporal_Mid_L","Temporal_Mid_R","Temporal_Pole_Mid_L","Temporal_Pole_Mid_R","Temporal_Pole_Sup_L","Temporal_Pole_Sup_R","Temporal_Sup_L","Temporal_Sup_R","Thalamus_L","Thalamus_R")

```
#ASD
```{r}
##ASD files load into R studio 
ASD <- read.csv("/home/ashley/Workspace/Subject_lists/ASD.csv",header=FALSE) #make header false so that it won't take away first subj
ASD <- data.frame(ASD) #change into a data frame 
colnames(ASD)<- c("Subj","Age") 
ASD<-ASD[order(ASD$Age),] #organizing data frame by ascending age
#print(ASD)
```
##corr matrices 
```{r}
for (i in rownames(ASD)) {
  asd_corr <- paste("Correlation_matrix_", ASD[i, "Subj"],".csv", sep="") #file name
  full_path <- paste("/home/ashley/Workspace/Correlation_matrices/",asd_corr, sep="") #path name 
  titles<- paste("ASD_","SUBJ_",ASD[i,"Subj"],"_","AGE_",ASD[i,"Age"],sep="") #title
  df_corr <- read.csv(full_path, header=FALSE) #assign it a name (df_corr) because you need to reference it when plotting 

  #Z-scores 
  zscore <- FisherZ(df_corr) #zscores from correlation matrices 
  zscore[zscore == Inf] <- NA #this changes all Inf values to NA 
  filename<-paste0("/home/ashley/Workspace/Z_scores/ASD/CSV_zscores/","ASD_","Subj_",ASD[i,"Subj"],"_","Age_",ASD[i,"Age"],"_zscore.csv")
  write_csv(zscore, filename)
  
  #plotting 
  rownames(ASD_clean)<-ROI_names 
   zscore_graphs<-print(ggcorrplot(zscore)+ 
   labs(title = titles))
  ggsave(zscore_graphs,filename=paste("ASD_Zscore_",ASD[i,"Subj"],".png",sep=""))
  
}
```
##avg by age 
```{r} 
#Averages of ASD and TD within each age group from 9-19 years old 
#ASD
for (u in 9:19) {
  csvs_u <- lapply(list.files(path="./Z_scores/ASD/", pattern=paste("*_ Age_", u, "_zscore.csv"), full.names = T), read.csv) #list files with this pattern and then read the csv file 
  Avg_ASD_u<-Reduce("+", csvs_u) / length(csvs_u) #to find the average of csv files by age 
  
  rownames(Avg_ASD_u)<-ROI_names
  colnames(Avg_ASD_u)<-ROI_names
  filenames<-paste("ASD_Avg_",u,".csv")
  
  write_csv(Avg_ASD_u, filenames)
  
  Avg_u<-print(ggcorrplot(Avg_ASD_u)+ 
  labs(title = paste("AVG_ASD_zscores_Age_",u,sep="")))
  ggsave(Avg_u,filename=paste("ASD_Avg_zscore_",u,".png",sep=""))
  
}
```  
#TD
```{r}
##TD load files into R studio 
TD <- read.csv("/home/ashley/Workspace/Subject_lists/TD.csv",header=FALSE) #make header false so that it won't take away first subj
TD <- data.frame(TD)
colnames(TD)<- c("Subj","Age") 
TD<-TD[order(TD$Age),] #organizing data frame by ascending age
#print(TD)
```
##corr matrices 
```{r}
for (i in rownames(TD)) {
  td_corr <- paste0("Correlation_matrix_", TD[i, "Subj"],".csv") #file name
  full_path <- paste0("/home/ashley/Workspace/Correlation_matrices/",td_corr)
  titles<- paste0("TD_","SUBJ_",TD[i,"Subj"],"_","AGE_",TD[i,"Age"]) #to make titles
  df_corr_td <- read.csv(full_path, header=FALSE)  
  
  
  ##TD r to z scores 
  zscore <- FisherZ(df_corr_td) #zscores from correlation matrices 
  zscore[zscore == Inf] <- NA #This makes all of the Inf (infinite values) equal to 1
  
  filename<-paste0("/home/ashley/Workspace/Z_scores/TD/CSV_zscores/",
                    "TD_","Subj_",TD[i,"Subj"],"_","Age_",TD[i,"Age"],
                    "_zscore.csv")
  write_csv(zscore, filename)
  
  colnames(TD_clean)<-ROI_names
  rownames(TD_clean)<-ROI_names 
  
  zscore_graphs<-print(ggcorrplot(TD_clean)+ 
  labs(title = titles))
     ggsave(zscore_graphs,filename=paste("TD_Zscore_",TD[i,"Subj"],".png",sep=""))
 }

```
##avg by age 
```{r}
#TD average of z-scores 
for (u in 9:19) {
  csvs_u <- lapply(list.files(path="./Z_scores/TD/", pattern=paste("*_ Age_", u, "_zscore.csv"), full.names = TRUE), read.csv) #list files with this pattern and then read the csv file 
  Avg_TD_u<-Reduce("+", csvs_u) / length(csvs_u) #to find the average of csv files by age 
  
  rownames(Avg_TD_u)<-ROI_names
  colnames(Avg_TD_u)<-ROI_names
  
  filenames<-paste("TD_Avg_",u,".csv")
  write_csv(Avg_TD_u, filenames)
  
  Avg_u<-print(ggcorrplot(Avg_TD_u)+ 
  labs(title = paste("AVG_TD_zscores_Age_",u,sep="")))
  ggsave(Avg_u,filename=paste("TD_Avg_zscore_",u,".png",sep=""))
}
```
