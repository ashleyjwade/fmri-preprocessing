%MODEL ESTIMATION, DETREND, BANDPASS FILTER, MASK GREY MATTER 
%Model Estimation 
for i=1:length(subj)
    batchname=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/first_level_estimate%d',subj(i));
    %SPMmatfilenames{i,1}=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/SPM_mat_files/%d/SPM.mat',subj(i));
    SPMmatfilenames(i,1)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/SPM_mat_files/%d/SPM.mat',subj(i))); %run this FOR loop first to create the SPMmatfilenames structure in the workspace, then move the 'end' function to the end of the call  
    %writing the (i,1) tells SPM what direction that you want the data to go
end

%into the cell (horizontally or vertically) 
    matlabbatch{1}.spm.stats.fmri_est.spmmat ={sprintf('%s/%s',SPMmatfilenames(i).folder,SPMmatfilenames(i).name)};   %changed matlabbatch{2} to matlabbatch{1} because it was creating a 1x2 cell with the first cell empty as it was ran before and now I am running the model estimation part separately 
    matlabbatch{1}.spm.stats.fmri_est.write_residuals = 1;
    matlabbatch{1}.spm.stats.fmri_est.method.Classical = 1;
    save(batchname,'matlabbatch')
end

%%Make a 'Residual' folder for each participant and move all of the 180
%%residuals to this folder 
for i=1:length(subj)
    currentFolder=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/SPM_mat_files/%d',subj(i));
    cd(currentFolder) %this function changes the directory to be in the current file 
    mkdir ../* Residuals 
    movefile Res_*.nii Residuals 
end  
    %to do one folder at a time
    %currentFolder=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/SPM.mat files/5288'));
    %mkdir ../5289 Residuals %makes a new folder in the current folder 
    %movefile Res_*.nii Residuals %moves all residuals into 'Residual folder'
%%
%Step 9 Detrend 
for i=1:length(subj)
    resdir=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/SPM_mat_files/%d/Residuals',subj(i));
    rp_detrend(resdir,'_detrend')
end 
%%    
%Step 10 Bandpass Filter 
for i=1:length(subj)
    detrendresdir=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/SPM_mat_files/%d/Residuals_detrend',subj(i)); 
    rp_bandpass(detrendresdir,2,0.08,0.009,'Yes',0)
end 
%%
%Step 11 Mask Grey Matter
for i=1:length(subj)
    %c1filenames=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/c1*_MPRAGE_256_*_*.nii',subj(i));
    c1filenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/c1*_MPRAGE_256_*_*.nii',subj(i)));
    fil4dvol=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/SPM_mat_files/%d/Residuals_detrend_filtered/Filtered_4DVolume.nii',subj(i));
    spm_mask(sprintf('%s/%s',c1filenames(i).folder,c1filenames(i).name),fil4dvol,0.001) 
end
