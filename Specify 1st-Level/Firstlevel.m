%-----------------------------------------------------------------------
% Job saved on 04-Oct-2021 18:47:51 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7487)
%----------------------------------------------------------------------
%this FOR LOOP is to make all of the necessary files (ar, C2, C3, rp, etc.)
%in the workspace, when a cell is created that means there needs to be many
%files (180) that need to be available, not just the 'ar files' but all 180
%ar files for each subject 

% add custome code for processing
%addpath("/home/.bml/Data/Bank1/Ashley/fsfMRI/custome_scripts")

sub_dir = '/home/.bml/Data/Bank1/Ashley/Codes/Subject_lists/';
load(strcat(sub_dir, 'allsubj.mat'));
subj = sub;
for i=1:length(subj)
    batchname=sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/All_1st_level/first_level%d',subj(i));
    preprocessedEPIfilenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/ar*_ep2d_moco_3mm_180_*_*.nii',subj(i)));
    %these are the new files that have already been preprocessed with
    %prefix 'ar'
    arEPI_files=cell(180,1);
    for u=1:180;
        arEPI_files{u}=sprintf('%s/%s,%d',preprocessedEPIfilenames(i).folder,preprocessedEPIfilenames(i).name,u);
    end 
    C2filenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/c2%d_MPRAGE_256_*_*.nii',subj(i),subj(i)));
    %the c2 files for each participant, when you use %d you need to
    %write subj(i) for each time you use %d
    C3filenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/c3%d_MPRAGE_256_*_*.nii',subj(i),subj(i)));
    %the c3 files for each participant 
    directoryfilenames = {};
    directoryfilenames(i,1)={sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d',subj(i))};
    %make a cell for the directory source (the first part of 1st level)
    rpfilenames(i)=dir(sprintf('/bml/Data/Bank1/Ashley/fsfMRI/Ashley_wade_rsfmri/Ashley_wade_rsfmri_nii/%d/rp_%d_ep2d_moco_3mm_180_*_*.txt',subj(i),subj(i)));
    %the rp files for each participant 
    WMCSF{1}=strcat(C2filenames(i).folder,'/',C2filenames(i).name); %we didn't write C2filenames(i) because we were just making sure the code would work for one participant at a time 
    WMCSF{2}=strcat(C3filenames(i).folder,'/',C3filenames(i).name);
    mask=strvcat(WMCSF); %this is to create the masks for each subject 
    
    % use custom script to get voxel values
    Ym_WM = catCarryingVoxel(strcat(C2filenames(i).folder, '/', C2filenames(i).name), arEPI_files, 1, 0.8);
    
    % original script using extract_voxel_Val
    %Ym_WM=extract_voxel_values(strcat(C2filenames.folder, '/', C2filenames.name),arEPI_files{1},'subj',0.8); %strcat is combining 2 strings together 
    %Ym=extract_voxel_values(mask,strvcat(arEPI_files{1:180}),'subj',0.8); %this is the original way to write with the 'mask'
    %Ym_WM = Ym(:,1);
    %Ym_CSF = Ym(:,2);
    Ym_CSF=catCarryingVoxel(strcat(C3filenames(i).folder,'/',C3filenames(i).name),arEPI_files,1,0.8);

    matlabbatch{1}.spm.stats.fmri_spec.dir = directoryfilenames(i,1);
    matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 34;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 2;
    %%
    matlabbatch{1}.spm.stats.fmri_spec.sess.scans = arEPI_files;
    %%
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi = {''};
    matlabbatch{1}.spm.stats.fmri_spec.sess.regress(1).name = 'WM';   
    matlabbatch{1}.spm.stats.fmri_spec.sess.regress(1).val = Ym_WM;
    %%
    matlabbatch{1}.spm.stats.fmri_spec.sess.regress(2).name = 'CSF';
    %%
    matlabbatch{1}.spm.stats.fmri_spec.sess.regress(2).val = Ym_CSF;
    %%
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = {sprintf('%s/%s',rpfilenames(i).folder,rpfilenames(i).name)};
    matlabbatch{1}.spm.stats.fmri_spec.sess.hpf = 128;
    matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
    matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
    matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
    matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
    matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
    save(batchname,'matlabbatch')
    
    %clearvars -except sub_dir subj i %when you clear after each subject it ensures you don't overwrite with the wrong files 
end 
