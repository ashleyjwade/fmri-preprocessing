# fMRI Preprocessing



## Step by step organization of resting state-fMRI preprocessing 
    Matlab* batch preparations in *Preprocessing *Specify 1st-level *AAL atlas
    R studio analyzations and graph preparations in R-Studio*


## Preprocessing
To get started with the preprocessing steps and all the detail is further explained in Copy_of_Functional_Connectivity_SOP.pdf (on main page)
-     1st step: Coregister: Estimate 
        This is to register an image with respect to another. With my rs-fMRI data, the reference image is: EPI and the source image is: T1
-     2nd step: Realign: Estimate & Reslice 
        There are different ways the brain is moving, and the fMRI machine is catching the images, so we need to realign the scans with eachother
-     3rd step: Slice Timing
         To correct for these slice-dependent delays, achieved by shifting the time series of each slice to temporally align all slices to a 
         reference time point
-     4th step: Segment 
         We do a special template for all of the subjects. We tell SPM to read all of our subjects T1 images and then the TPM (averaged brain 
         images), so then it will make a unique TPM based on our subject's T1 image 


##Specify 1st level 
In the 1st level analysis we will be obtaining the residual images, so we will regress out the things that we don't want and preserve the things that we do want 
-     Cat carrying voxel
         This script is designed to extract voxel values. There are 3 different options and read the script carefully to decide which option to 
         choose. This script is written by the Phd student- Yu-Shiang 
-     First level
         With the preprocessed files, we will use the ar, c2, c3, rp named files to create the first level mat file
-     Residuals
         This script will create a new folder called "Residuals" and then organize the residual niifti files there. In this script, we will have SPM 
         write the Residuals, take away high and low frequencies with 'bandpass filter', and mask the grey matter 


##AAL atlas
This is the atlas that is used for my rs-fMRI analysis. The version that is used here is AAL atlas (very first version), it can be downloaded [here](https://www.gin.cnrs.fr/en/tools/aal/)
Once the atlas is downloaded, create a folder for each subject with the ROI's, in my case there are 90 ROIs. Here is the list of ROIs used and orgaized into different cortical categories [here](https://docs.google.com/spreadsheets/d/1XXl37k5c-erTBxoFGsMK0URj_d47oacbSLKO7Tsigq8/edit?usp=sharing) 
-     ROI masks
         This is using the ROI masks from AAL atlas/roi_list.csv to make the masks by name and number
-     ROIs to subj
         This applies the masks to the subject's space and writes 'w' before the ROI name 
-     Extract Voxel Values
         This is taking all of the 180 residual files and the new 90 ROI files that have been deformed to the subject's space and extracting the
         voxel values. This will save a 180x90 matrix 

##Statistics applied to data 
These are the statistic scripts that are used in Matlab and R Studio to perform correlation matrices, converting to zscore, performing the t-test
-     Correlation matrices - matlab
        This is taking the 180x90 matrix made from the residual files and correlating each ROI with itself. There are 90 ROIs here so there will 
         be a 90x90 matrix at the end 
-     Alphabetical Correlations - R
         This loads the correlation matrices files for ASD and TD into R studio, then names the ROIs by column and by row (90x90 matrix), converts 
         the correlation matrices to zscore using 'FisherZ' and create a plot using 'ggcorrplot'. This script also combines the zscores by age for
         each group (ages 9-19 for ASD and TD) and creates a plot to show the age group differences 
-     Cortical order Correlations - R
         This takes the new cortical order list found in the same link from above [](https://docs.google.com/spreadsheets/d/
         1XXl37k5c-erTBxoFGsMK0URj_d47oacbSLKO7Tsigq8/edit?usp=sharing) to move the order of the correlation matrices, converting
         to z score and group by age and take the average from the age groups (9-19 years old)
-     T-test with new Cortical order - R
         This is using the cortical order and preparing the data for t-test. Each group (ASD and TD) has a 90x90 matrix of zscores. Now we want to 
         conduct the t-test for each element within the 90x90 matrix. At the end we will have a 90x90 t-test value and with this script the p-value
         is specified and saved into a .csv file 
-     FDR_Heatmaps - R
         This is taking the pvalue from the t-test and putting it into a multiple comparisons test to see which pvalues are "true" and not just a
         result from the correlation matrix or other factors. After the fdr test showing pvalue (p<0.05) is accepted, then a list of where the 
         significant pvalues are located in the 90x90 matrix are stored in a list. Using these locations to grab the t-values at the same locations 
         and putting it into one matrix. Once this is completed the t-values are put into a heatmap using 'pheatmap'
     


```
#To use terminal command for updating scripts
cd existing_repo
git remote add origin https://gitlab.com/ashleyjwade/fmri-preprocessing.git
git branch -M main
git push -uf origin main
```


